package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button zero;
    private Button one;
    private Button two;
    private Button three;
    private Button four;
    private Button five;
    private Button six;
    private Button seven;
    private Button eight;
    private Button nine;
    private Button ten;
    private Button add;
    private Button sub;
    private Button mul;
    private Button div;
    private TextView infoview;
    private TextView resview;
    private char action;
    private Button eqls;
    private Button clear;

    //Creating Constants for arithmaric operation
    private final char ADDITION='+';
    private final char SUBSTRACTION='-';
    private final char DIVISION='/';
    private final char MULTIPLICATION='*';
    private final char EQL=0;

    //CREATING A VARIABLES FOR HOLDING VALUES
    private double val1=Double.NaN;         //Represents a value that is not a number (NaN)
    private double val2;



    @Override
    //First Method to Call When Program get started is onCreate()
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupUiViews();
        //Creating On click Listeners........
        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoview.setText(infoview.getText().toString()+"0");
            }
        });
        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoview.setText(infoview.getText().toString()+"1");
            }
        });
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoview.setText(infoview.getText().toString()+"2");
            }
        });
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoview.setText(infoview.getText().toString()+"3");
            }
        });
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoview.setText(infoview.getText().toString()+"4");
            }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoview.setText(infoview.getText().toString()+"5");
            }
        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoview.setText(infoview.getText().toString()+"6");
            }
        });
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoview.setText(infoview.getText().toString()+"7");
            }
        });
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoview.setText(infoview.getText().toString()+"8");
            }
        });
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoview.setText(infoview.getText().toString()+"9");
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                action=ADDITION;
                resview.setText(String.valueOf(val1)+"+");
                infoview.setText(null);
            }
        });
        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                action=SUBSTRACTION;
                resview.setText(String.valueOf(val1)+"-");
                infoview.setText(null);
            }
        });
        div.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                action=DIVISION;
                resview.setText(String.valueOf(val1)+"/");
                infoview.setText(null);
            }
        });
        mul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                action=MULTIPLICATION;
                resview.setText(String.valueOf(val1)+"X");
                infoview.setText(null);
            }
        });
        eqls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                compute();
                action=EQL;
                resview.setText(resview.getText().toString()+String.valueOf(val2)+" = "+ String.valueOf(val1).toString());
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(infoview.getText().length()>0){
                    /*A CharSequence is a readable sequence of char values
                    * Name is the object of CharSequence*/
                    CharSequence name= infoview.getText().toString();
                    infoview.setText(name.subSequence(0,name.length()-1));
                }
                else{
                    val1=Double.NaN;
                    val2=Double.NaN;
                    infoview.setText(null);
                    resview.setText(null);
                }
            }
        });
    }

    private void setupUiViews(){
        //Assigning  Variables to Button........
        zero=(Button)findViewById(R.id.btn0);
        one=(Button)findViewById(R.id.btn1);
        two=(Button)findViewById(R.id.btn2);
        three=(Button)findViewById(R.id.btn3);
        four=(Button)findViewById(R.id.btn4);
        five=(Button)findViewById(R.id.btn5);
        six=(Button)findViewById(R.id.btn6);
        seven=(Button)findViewById(R.id.btn7);
        eight=(Button)findViewById(R.id.btn8);
        nine=(Button)findViewById(R.id.btn9);
        add=(Button)findViewById(R.id.btnadd);
        sub=(Button)findViewById(R.id.btnsub);
        mul=(Button)findViewById(R.id.btnmul);
        div=(Button)findViewById(R.id.btndiv);
        eqls=(Button)findViewById(R.id.btneq);
        clear=(Button)findViewById(R.id.btnclr);
        infoview=(TextView) findViewById(R.id.infoviews);
        resview=(TextView)findViewById(R.id.resviews);

    }

    //This Method having all Computations
    private void compute(){
        /* Since we  defined Val1 as (NaN) Not A Number, so we can get Val2 which is a number*/
        if(!Double.isNaN(val1)){
            val2=Integer.parseInt(infoview.getText().toString());
            switch(action){
                case ADDITION:
                    val1=val1+val2;
                    break;
                case SUBSTRACTION:
                    val1=val1-val2;
                    break;
                case DIVISION:
                    val1=val1/val2;
                    break;
                case MULTIPLICATION:
                    val1=val1*val2;
                    break;
                case EQL:
                    break;
            }
        }
        else{
            val1=Double.parseDouble(infoview.getText().toString());
        }
    }
}
